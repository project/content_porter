One of the challenges in a long running Drupal implementations is having to move 
entities created on lower environments as part of regular feature development to 
higher environments including production environment credibly where regular 
database movement is not an option. Content porter module helps to move partial 
content updates(and other entities) to different environments without the need 
of moving complete Database. Use content porter to deploy entities like node, 
taxonomies, files and users from one Drupal environment to another. 
Content porter automatically manages all dependencies like reference fields. 
Or you can override this behaviour and choose not to migrate the references. 
Content porter creates export packages of entities which are zip files containing json files. 
These packages can then be imported to another environment where content porter is enabled.

## Features:

*   Simple export/import option.
*   Export downloadable zip file packages
*   Support for almost all entities.
*   Exported packages can be imported from the interface
*   Supports Paragraph.
*   Can download individual/bulk content/taxonomy.
*   Option to deploy/skip reference fields
*   Easy installation

## Important Notes:

*   If module does not find the user by which entity was created, then it creates its own user(system_migration) and imports the content by it.
*   To export and import, source and target environment should have same structure.

## Upcoming features:

*   Support for physical image deployment
*   Support of the field mapping at the time of package import for more flexibility