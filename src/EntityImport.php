<?php

namespace Drupal\content_porter;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\entity_reference_revisions\EntityReferenceRevisionsFieldItemList;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\user\Entity\User;
use Drupal\taxonomy\Entity\Term;

/**
 * Import entity.
 */
class EntityImport {

  use EntityCommonTrait;

  /**
   * Drupal serializer variable.
   *
   * @var serializer
   */
  public $serializer = NULL;

  /**
   * Content entity array.
   *
   * @var array
   */
  public $contentEntities;
  /**
   * Entity reference array.
   *
   * @var array
   */
  public $loadedEntityReferences = [];

  /**
   * Total files to process.
   *
   * @var int
   */
  public $totalFiles = 0;

  /**
   * Total list of files in folder.
   *
   * @var int
   */
  public $fileList = 0;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $jsonConfig;

  /**
   * Create folder name run time.
   *
   * @var array
   */
  private $folderName = [];

  /**
   * Constructor.
   */
  public function __construct($folderName) {
    $this->setConfigZipPath();
    $this->serializer = \Drupal::service('serializer');
    $this->folderName = $folderName;
    if (!empty($folderName)) {
      $this->configFilePath = $this->configFilePath . '/' . $this->folderName;
    }
    // Create key value of all content entities in Drupal.
    $this->loadContentEntities();
    // Get json config to process data.
  }

  /**
   * Initiate import from package.
   */
  public function import() {
    // Load files from direcotry.
    $this->loadFiles();
  }

  /**
   * Sort files before sorting.
   */
  public function sortFiles() {
    $glob = new \GlobIterator($this->configFilePath . '/*.json');
    $files = [];
    foreach ($glob as $current) {
      $this->totalFiles++;
      $filename = $current->getFilename();
      if (strpos($filename, '.default')) {
        array_unshift($files, $filename);
      }
      else {
        $files[] = $filename;
      }
    }

    // Set files array as global fileList.
    $this->fileList = $files;
    return $files;
  }

  /**
   * Load all extracted files.
   */
  public function loadFiles() {
    $files = $this->sortFiles();
    foreach ($files as $file) {
      $split_name = explode('.', $file);
      $this->readFile($file, $split_name[1]);
    }
  }

  /**
   * Read file data one by one.
   */
  public function readFile($filename, $type) {
    $data = file_get_contents($this->configFilePath . '/' . $filename);
    $data_array = $this->parseJsonContent($data);

    // If data is not empty in file.
    if (!empty($data_array)) {
      $entity = \Drupal::service('entity.repository')->loadEntityByUuid($type, $data_array['uuid'][0]['value']);

      // Check if username exist.
      if ($type == 'user' && !$entity) {
        $entity = user_load_by_name($data_array['name'][0]['value']);
      }

      // If entity is already exist then update.
      if ($entity) {
        $entity = $this->getEntityTraslation($entity, $data_array);
        $this->updateNewRecords($entity, $data_array);
      }

      // Create entity of not exist.
      else {
        $entity = $this->createEntity($data_array, $type);
      }

      // Update stack record.
      if ($entity) {
        $this->loadedEntityReferences[$entity->uuid() . '.' . $type] = ['id' => $entity->id(), 'revision_id' => $entity->getRevisionId()];
        return $entity;
      }
    }
  }

  /**
   * Return field name from JSON configuration rules for term update.
   */
  public function taxonomyGetFieldNameJsonRule($dataValue, $existing_vid) {

    $fieldName = NULL;
    foreach ($dataValue as $key => $value) {
      if ($value->vid == $existing_vid) {
        $fieldName = $value->field_name;
        break;
      }
    }
    return $fieldName;
  }

  /**
   * Update taxonomy term using json configuration.
   */
  public function updatEntityUsingJsonConfig($data_array, $type) {

    // Process data on the basis of json config.
    $allowed_teypes = ['taxonomy_term', 'node', 'paragraph'];
    $existing_vid = $data_array['vid'][0]['target_id'];
    $fieldName = NULL;
    $matchFound = FALSE;

    foreach ($this->json_config_obj as $key => $dataValue) {
      if (in_array($key, $allowed_teypes) && $key == 'taxonomy_term') {
        $fieldName = $this->taxonomyGetFieldNameJsonRule($dataValue, $existing_vid);
        break;
      }
    }
    if (!empty($data_array[$fieldName])) {
      $sourceFieldValue = $data_array[$fieldName][0]['value'];
      $eq = \Drupal::entityQuery('taxonomy_term');
      $eq->condition('vid', $existing_vid);
      $eq->condition($fieldName, $sourceFieldValue);
      $terms = $eq->execute();

      foreach ($terms as $term) {
        $destEntity = Term::load($term);
        $matchFound = TRUE;
        // Update $data_array parent uuid with existing parent uuid.
        if (!empty($data_array['parent'])) {
          $storage = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');
          $parents = $storage->loadParents($term);
          $parent_arr = [];
          foreach ($parents as $parent) {
            $parent_arr[] = [
              'target_id' => $parent->id(),
              'target_uuid' => $parent->uuid(),
            ];
          }
          $data_array['parent'][0]['target_uuid'] = $parent_arr[0]['target_uuid'];
        }
        break;
      }
      // Update term if field value match found.
      if ($matchFound) {
        $data_array['uuid'][0]['value'] = $destEntity->uuid();
        $entity = $this->getEntityTraslation($destEntity, $data_array);
        $this->updateNewRecords($entity, $data_array);
        $this->loadedEntityReferences[$entity->uuid() . '.' . $type] = ['id' => $entity->id(), 'revision_id' => $entity->getRevisionId()];
        return $entity;
      }
      // Create taxonomy term.
      else {
        $entity = $this->createEntity($data_array, $type);
        $this->loadedEntityReferences[$entity->uuid() . '.' . $type] = ['id' => $entity->id(), 'revision_id' => $entity->getRevisionId()];
        return $entity;
      }
    }
    return $matchFound;
  }

  /**
   * Get entity translation for particular langcode else create it.
   */
  public function getEntityTraslation($entity, $data_array) {
    $langcode = $data_array['langcode']['0']['value'];
    if (!$entity->hasTranslation($langcode)) {
      $entity = $entity->addTranslation($langcode);
    }
    else {
      $entity = $entity->getTranslation($langcode);
    }
    return $entity;
  }

  /**
   * Update record of already existed entity.
   */
  protected function updateNewRecords($original_entity, $data_array) {
    $serializeClass = $this->contentEntities[$original_entity->getEntityTypeId()];
    $entity = $this->serializer->deserialize(json_encode($data_array), $serializeClass, 'json');
    // Overwrite the received properties.
    $entity_keys = $entity->getEntityType()->getKeys();
    foreach ($entity->_restSubmittedFields as $field_name) {
      $field = $entity->get($field_name);

      if ($field instanceof EntityReferenceRevisionsFieldItemList) {
        $original_entity->set($field_name, $data_array[$field_name]);
        continue;
      }

      // Entity key fields need special treatment: together they uniquely
      // identify the entity. Therefore it does not make sense to modify any of
      // them. However, rather than throwing an error, we just ignore them as
      // long as their specified values match their current values.
      if (in_array($field_name, $entity_keys, TRUE)) {
        // @todo Work around the wrong assumption that entity keys need special
        // treatment, when only read-only fields need it.
        // This will be fixed in https://www.drupal.org/node/2824851.
        if ($entity->getEntityTypeId() == 'comment' && $field_name == 'status' && !$original_entity->get($field_name)->access('edit')) {
          throw new AccessDeniedHttpException("Access denied on updating field '$field_name'.");
        }

        // Unchanged values for entity keys don't need access checking.
        if ($this->getCastedValueFromFieldItemList($original_entity->get($field_name)) === $this->getCastedValueFromFieldItemList($entity->get($field_name))) {
          continue;
        }
        // It is not possible to set the language to NULL as it is automatically
        // re-initialized. As it must not be empty, skip it if it is.
        elseif (isset($entity_keys['langcode']) && $field_name === $entity_keys['langcode'] && $field->isEmpty()) {
          continue;
        }
      }
      $original_entity->set($field_name, $field->getValue());
    }
    $this->updateEntityUser($original_entity);

    if ($original_entity->hasField('parent') && ($entity instanceof Term) && empty($entity->parent->getValue())) {
      $original_entity->parent = [0];
    }
    try {

      // Set revision creation time for entity type node.
      if ($original_entity->getEntityTypeId() == 'node') {
        $this->setRevisionInfo($original_entity, 'Updated');
      }

      $original_entity->save();

      // Log report in file.
      $message = t("[ Entity updated of type @type - @lable(@id) ]\n", [
        '@type' => $original_entity->getEntityTypeId(),
        '@lable' => $original_entity->label(),
        '@id' => $original_entity->id(),
      ]);
      Util::createDataLog($message, 'entity_package_upload_', $this->folderName);
    }
    catch (\Exception $e) {
      $message = t("[ Error in updating entity:- \n@message \nentity:@entity \nuuid:@uuid ]\n", [
        '@message' => $e->getMessage(),
        '@entity' => $original_entity->label(),
        '@uuid' => $original_entity->uuid(),
      ]);
      Util::createDataLog($message, 'entity_package_upload_', $this->folderName);
    }
  }

  /**
   * Set revision information for entity.
   */
  public function setRevisionInfo($entity, $op) {
    $request_time = \Drupal::time();
    $user = user_load_by_name('system_migration');
    $entity->setRevisionCreationTime($request_time->getRequestTime());
    $entity->setRevisionLogMessage(t("Node @op by porter utility on @dated.",
        ['@op' => $op, '@dated' => date("m/d/Y - H:i", time())]));
    if ($user) {
      $entity->setRevisionUser($user);
    }
  }

  /**
   * Update any entity with system_migration user.
   */
  protected function updateEntityUser($entity) {
    if ($entity->hasField('uid')) {
      $query = \Drupal::entityQuery('user');
      $query->condition('name', 'system_migration')->range(0, 1);
      $user_id = $query->execute();
      if (!empty($user_id)) {
        $user = User::load(reset($user_id));
      }
      else {
        $user = User::create();
        $user->setPassword('system_migration');
        $user->enforceIsNew();
        $user->setEmail('system_migration@gmail.com');
        $user->setUsername('system_migration');

        // Optional.
        $user->set('init', 'system_migration@gmail.com');
        $user->set('langcode', 'en');
        $user->set('preferred_langcode', 'en');
        $user->set('preferred_admin_langcode', 'en');
        $user->activate();
        // Save user account.
        $user = $user->save();
      }
      if ($user && !($entity instanceof User)) {
        $entity->set('uid', $user);
      }
    }
  }

  /**
   * Parse reference entity if exist in entity.
   */
  protected function referneceFile($uuid, $type) {
    $glob = new \GlobIterator($this->configFilePath . '/' . $uuid . '.' . $type . '*.json');
    if ($glob && !empty($glob->getFilename())) {
      $this->readFile($glob->getFilename(), $type);
    }
  }

  /**
   * Create entity if not exist.
   */
  public function createEntity($data, $type) {
    $serializeClass = $this->contentEntities[$type];
    $entity = $this->serializer->deserialize(json_encode($data), $serializeClass, 'json');
    $this->preSaveEntityField($entity, $data);
    $this->updateEntityUser($entity);
    if ($entity->getEntityTypeId() == 'node') {
      $this->setRevisionInfo($entity, 'Created');
    }
    if ($entity->hasField('parent') && ($entity instanceof Term) && empty($entity->parent->getValue())) {
      $entity->parent = [0];
    }

    try {
      $entity->save();

      $this->getEdLogger()->alert('entity created: @type:@id', ['@type' => $entity->id(), '@id' => $entity->getEntityTypeId()]);

      // Log report in file.
      $message = t("[ Created entity of type @type - @lable(@id) ]\n", [
        '@type' => $entity->getEntityTypeId(),
        '@lable' => $entity->label(),
        '@id' => $entity->id(),
      ]);
      Util::createDataLog($message, 'entity_package_upload_', $this->folderName);
    }
    catch (\Exception $e) {
      $message = t("[ Error in creating entity:- \n@message \nentity:@entity \nuuid:@uuid ]\n", [
        '@message' => $e->getMessage(),
        '@entity' => $entity->label(),
        '@uuid' => $entity->uuid(),
      ]);
      Util::createDataLog($message, 'entity_package_upload_', $this->folderName);
    }

    return $entity;
  }

  /**
   * Update record before entity save.
   */
  protected function preSaveEntityField($entity, $data_array) {
    foreach ($entity->_restSubmittedFields as $field_name) {
      $field = $entity->get($field_name);
      if ($field instanceof EntityReferenceRevisionsFieldItemList) {
        $entity->set($field_name, $data_array[$field_name]);
      }
    }
  }

  /**
   * Parse json content in file.
   */
  public function parseJsonContent($data) {

    $entity_arr = json_decode($data, TRUE);
    foreach ($entity_arr as $field_name => &$entity_field) {
      foreach ($entity_field as &$entity_field_val) {
        $this->parseFieldContent($entity_field_val);
      }
    }
    return $entity_arr;
  }

  /**
   * If refernece entity is exist in entity then update it.
   */
  public function parseFieldContent(&$fieldVals) {
    if (isset($fieldVals['target_id']) && isset($fieldVals['target_type']) && isset($this->contentEntities[$fieldVals['target_type']])) {
      $this->referneceFile($fieldVals['target_uuid'], $fieldVals['target_type']);

      if (isset($this->loadedEntityReferences[$fieldVals['target_uuid'] . '.' . $fieldVals['target_type']])) {
        $target_entity_info = $this->loadedEntityReferences[$fieldVals['target_uuid'] . '.' . $fieldVals['target_type']];

        $fieldVals['target_id'] = $target_entity_info['id'];

        // Handel paragraph.
        if (isset($fieldVals['target_revision_id'])) {
          $fieldVals['target_revision_id'] = $target_entity_info['revision_id'];
        }
      }
    }
  }

  /**
   * Get list of all avaiable content type entities.
   */
  public function loadContentEntities() {
    $entity_type_definations = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($entity_type_definations as $definition) {
      if ($definition instanceof ContentEntityType) {
        $this->contentEntities[$definition->id()] = $definition->getClass();
      }
    }
  }

  /**
   * Gets the values from the field item list casted to the correct type.
   *
   * Values are casted to the correct type so we can determine whether or not
   * something has changed. REST formats such as JSON support typed data but
   * Drupal's database API will return values as strings. Currently, only
   * primitive data types know how to cast their values to the correct type.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field_item_list
   *   The field item list to retrieve its data from.
   *
   * @return mixed[][]
   *   The values from the field item list casted to the correct type. The array
   *   of values returned is a multidimensional array keyed by delta and the
   *   property name.
   */
  protected function getCastedValueFromFieldItemList(FieldItemListInterface $field_item_list) {
    $value = $field_item_list->getValue();

    foreach ($value as $delta => $field_item_value) {
      /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
      $field_item = $field_item_list->get($delta);
      $properties = $field_item->getProperties(TRUE);
      // Foreach field value we check whether we know the underlying property.
      // If we exists we try to cast the value.
      foreach ($field_item_value as $property_name => $property_value) {
        if (isset($properties[$property_name]) && ($property = $field_item->get($property_name)) && $property instanceof PrimitiveInterface) {
          $value[$delta][$property_name] = $property->getCastedValue();
        }
      }
    }

    return $value;
  }

}
