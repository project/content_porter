<?php

namespace Drupal\content_porter;

use Drupal\Core\Site\Settings;

/**
 * Util.
 */
class Util {
  /**
   * Function to update logs and make directory structure for inserting logs.
   *
   * @param mixed $data
   *   Message to print in the logs.
   * @param string $file
   *   Filename without extension.
   * @param string $file_identity
   *   File Identity expression.
   */

  /**
   * Creating logs.
   */
  public static function createDataLog($data, $file = 'default_log', $file_identity = NULL) {
    $structure = Settings::get('application_log_folder', 'sites/default/files/application_logs/');

    if (!is_dir($structure)) {
      mkdir($structure, 0755, TRUE);
    }
    $content_to_write = '';
    if (is_array($data)) {
      foreach ($data as $value) {
        $content_to_write .= $value . " \n";
      }
    }
    else {
      $content_to_write .= $data . " \n";
    }
    if ($file_identity) {
      $file_name = $file . date("Y_m_d_H") . '_' . $file_identity . '.txt';
    }
    else {
      $file_name = $file . date("Y_m_d_H") . '_' . date('h_i') . '.txt';
    }

    $fp = fopen($structure . $file_name, "a");
    \Drupal::logger($file)->info($content_to_write);
    fwrite($fp, $content_to_write);
    unset($content_to_write);
  }

}
