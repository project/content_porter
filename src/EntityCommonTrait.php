<?php

namespace Drupal\content_porter;

/**
 * Common functinality trait.
 */
trait EntityCommonTrait {
  /**
   * Entity export users config.
   *
   * @var string
   */
  protected $exportUsers;

  /**
   * Entity import/export path.
   *
   * @var string
   */
  protected $configFilePath;

  /**
   * Path to create zip file.
   *
   * @var string
   */
  protected $zipFilePath;

  /**
   * Get export config path.
   */
  public function getConfigFilePath() {
    return $this->configFilePath;
  }

  /**
   * Get config object.
   *
   * @return object Drupal setting object.
   */

  /**
   * Config.
   */
  public function getConfig() {
    return \Drupal::state();
  }

  /**
   * Set export config path.
   */
  public function setConfigZipPath($zipFilePath = '') {
    $config = $this->configs = $this->getConfig();
    if (!empty($config->get('content_porter.zipFilePath'))) {
      $zipFilePath = $config->get('content_porter.zipFilePath');
    }
    else {
      $zipFilePath = \Drupal::service('file_system')->realpath(file_default_scheme() . "://");
    }

    $this->zipFilePath = DRUPAL_ROOT . "/" . $zipFilePath;
    $this->exportUsers = $config->get('content_porter.exportUsers', 0);
    $this->configFilePath = DRUPAL_ROOT . "/" . $zipFilePath . "/packages";
  }

  /**
   * Get zip export config path.
   */
  public function getConfigZipPath() {
    return $this->zipFilePath;
  }

  /**
   * Provides the 'system' channel logger service.
   */
  public function getEdLogger() {
    return \Drupal::logger('content_porter');
  }

}
