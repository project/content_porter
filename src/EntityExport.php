<?php

namespace Drupal\content_porter;

use Drupal\Core\Entity\EntityInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\link\Plugin\Field\FieldType\LinkItem;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\paragraphs\Entity\Paragraph;

/**
 * Export entity and reference entities.
 */
class EntityExport {

  use EntityCommonTrait;

  /**
   * Var to store content.
   *
   * @var object
   */
  protected $content = NULL;

  /**
   * SyncContent To store the config info from yml.
   *
   * @var array
   */
  private $config = NULL;

  /**
   * SyncContent To store the config info from yml.
   *
   * @var array
   */
  private $processed = [];

  /**
   * Create folder name run time.
   *
   * @var array
   */
  private $folderName = [];

  /**
   * Set the content to sync.
   */
  public function setContent(EntityInterface $content) {
    $this->content = $content;
    if (!is_dir($this->configFilePath)) {
      // Directory does not exist, so lets create it.
      if (!mkdir($this->configFilePath, 0755, TRUE)) {
        die('Failed to create <b>packages</b> folders at ' . $this->configFilePath . 'Please set write permission to files folder.');
      }

    }

    return $this;
  }

  /**
   * Constructor to load default settings.
   */
  public function __construct($folderName = '') {
    // $this->setConfigFilePath();
    $this->setConfigZipPath();
    // Load multisite settings.
    $this->folderName = $folderName;
    if (!empty($this->folderName)) {
      $this->configFilePath = $this->configFilePath . '/' . $this->folderName;
    }
  }

  /**
   * Parse content data.
   *
   * @param \Drupal\Core\Entity\EntityInterface $data
   *   EntityInterface Object.
   */
  public function parseContent(EntityInterface $data = NULL) {
    $content = ($data) ? $data : $this->content;

    if (isset($content->field_meta_experience_type)) {
      unset($content->field_meta_experience_type);
    }

    foreach ($content as $key => $field_objs) {
      $i = 0;
      foreach ($field_objs as $fieldItem) {

        try {

          if (($fieldItem instanceof LinkItem && $entity = $this->processLink($fieldItem)) || ($entity = $fieldItem->get('entity')->getValue())) {

            // Export entity without reference entites but with paragraph.
            if ($this->getConfig()->get('content_porter.withoutReference') && (!$entity instanceof Paragraph)) {
              continue;
            }

            if (!$entity instanceof ContentEntityInterface) {
              continue;
            }

            foreach ($entity->getTranslationLanguages() as $id => $value) {
              $this->parseContent($entity->getTranslation($id));
              $this->send($entity);
            }

            $i++;
          }
        }
        catch (\Exception $e) {

          $this->getEdLogger()->warning('@message', ['@message' => $e->getMessage()]);
        }
      }
    }
  }

  /**
   * To remove black list data from node array.
   *
   * @param object $content
   *   Content array to remove unwanted fields.
   * @param string $type
   *   Content array to remove unwanted fields.
   *
   * @return array
   *   Filtered content array.
   */
  public function removeVars($content, $type) {
    // Blacklist specific to entity.
    $black_list['node'] = ['vid', 'nid'];
    $black_list['embridge_asset_entity'] = ['id'];
    $black_list['paragraph'] = ['id', 'parent_id'];
    $black_list['user'] = ['uid'];
    $black_list['field_collection_item'] = ['item_id'];
    $black_list['taxonomy_term'] = ['tid', 'default_langcode'];
    $black_list['file'] = ['fid'];

    // Blacklist for all entities.
    $black_list['common'] = [
      'changed',
      'uid',
      'field_assigned_to',
      'field_assign_to_non_cms_user',
      // 'langcode',
      // 'uuid',.
      'revision_timestamp',
      'revision_translation_affected',
      'revision_uid',
      'content_translation_source',
      'content_translation_outdated',
      'content_translation_uid',
      'content_translation_status',
      'content_translation_created',
      '_rev',
      'revision_id',
    ];

    if (isset($black_list[$type])) {
      $black_lists = array_merge($black_list['common'], $black_list[$type]);
    }
    else {
      $black_lists = $black_list['common'];
    }

    // Loop to remove black list data from content.
    foreach ($black_lists as $bl) {
      if (isset($content->{$bl})) {
        unset($content->{$bl});
      }
    }
    return $content;
  }

  /**
   * Method to process link field.
   *
   * @param Drupal\link\Plugin\Field\FieldType\LinkItem $linkField
   *   Link field.
   */
  public function processLink(LinkItem $linkField) {
    $entity = NULL;
    // Check if uri is entity.
    if (isset($linkField->uri) && preg_match('/entity:/', $linkField->uri)) {
      // Replace entity tag in URI.
      $entity_string = preg_replace('/entity:/', '', $linkField->uri);
      $entity_arr = explode('/', $entity_string);
      // Check and Load Entity.
      if (isset($entity_arr[0]) && isset($entity_arr[1])) {
        $entity = \Drupal::entityTypeManager()->getStorage($entity_arr[0])->load($entity_arr[1]);
      }
    }
    return $entity;
  }

  /**
   * Get Entity id by id or key.
   */
  public function getEntityByIdOrKey($field_name, $entity_type, $field_value) {
    if (is_numeric($field_value)) {
      $entity_id = $field_value;
    }
    else {
      $eq = \Drupal::entityQuery($entity_type);

      if ($field_name == 'query_params') {
        $eq->condition('field_dynamic_params', $field_value);
      }
      elseif (in_array($field_name, ['view_type', 'style'])) {
        $eq->condition('field_component_identifier', $field_value);
      }
      $eq->range(0, 1);
      $entity_id = reset($eq->execute());
    }
    return \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);
  }

  /**
   * Sync data across multisites domains using rest service.
   *
   * @param object $entity
   *   Content array which to be send.
   */
  public function send($entity) {
    // Return if entity already processed.
    if (isset($this->processed[$entity->uuid()])) {
      return;
    }

    $serializer = \Drupal::service('serializer');
    $langcode = $entity->language()->getId();
    $default_lang = $entity->isDefaultTranslation() ? '.default' : '';
    $file_name = $entity->uuid() . '.' . $entity->getEntityTypeId() . '.' . $langcode . $default_lang . '.json';

    // Convert entity to json object.
    $content = $serializer->serialize($entity, 'json');

    // Convert json object to array object.
    $content_obj = json_decode($content);

    // Do not process user.
    if ($entity->getEntityTypeId() == 'user' && !$this->exportUsers) {
      return;
    }

    // Handle taxonomy term parent.
    if (($entity instanceof Term) && $entity->hasField('parent') && !isset($this->processed[$entity->uuid()])) {

      $storage = \Drupal::service('entity_type.manager')->getStorage('taxonomy_term');
      $parents = $storage->loadParents($entity->id());
      $parent_arr = [];
      foreach ($parents as $parent) {
        $parent_arr[] = [
          'target_id' => $parent->id(),
          'target_type' => 'taxonomy_term',
          'target_uuid' => $parent->uuid(),
        ];
      }
      $content_obj->parent = $parent_arr;
    }

    // Return if file already created.
    if (file_exists($this->configFilePath . '/' . $file_name)) {
      return;
    }

    // Removed black list content from object.
    $filtered_obj = $this->removeVars($content_obj, $entity->getEntityTypeId());
    // Update target ids of remote fields entity.
    $content_json = \GuzzleHttp\json_encode($filtered_obj, JSON_PRETTY_PRINT);

    try {
      file_put_contents($this->configFilePath . '/' . $file_name, $content_json);
      $this->processed[$entity->uuid()] = $entity->uuid();
    }
    catch (RequestException $e) {
      $this->getEdLogger()->warning('@message', ['@message' => $e->getMessage()]);
    }
    catch (\Exception $e) {
      $this->getEdLogger()->warning('@message', ['@message' => $e->getMessage()]);
    }
  }

  /**
   * Method to handle contents.
   */
  public function export() {

    $this->parseContent();
    $this->send($this->content);

    return $this;
  }

}
