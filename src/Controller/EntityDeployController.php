<?php

namespace Drupal\content_porter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\content_porter\EntityCommonTrait;

/**
 * Controller to handle download request.
 */
class EntityDeployController extends ControllerBase {

  use EntityCommonTrait;

  /**
   * Start downloading using this method.
   *
   * @param string $package_id
   *   Package id.
   */
  public function download($package_id) {
    $this->setConfigZipPath();
    $zip_name = 'content-package_' . $package_id . '.zip';
    $zip_path = $this->getConfigZipPath() . '/' . $zip_name;

    if (file_exists($zip_path)) {
      header('Content-Description: File Transfer');
      header('Content-Type: application/octet-stream');
      header('Content-Disposition: attachment; filename="' . basename($zip_path) . '"');
      header('Expires: 0');
      header('Cache-Control: must-revalidate');
      header('Pragma: public');
      header('Content-Length: ' . filesize($zip_path));
      readfile($zip_path);
      exit;
    }
    else {
      return ['#markup' => $this->t('No package found for download.')];
    }
  }

}
