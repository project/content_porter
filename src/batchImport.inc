<?php

/**
 * @file
 * Use to sync vocabolaries and terms to child sites.
 */

use Drupal\Core\Render\Markup;
use Drupal\content_porter\EntityImport;
use Drupal\Core\Site\Settings;
use Drupal\content_porter\Util;

/**
 * Function to check finish status.
 *
 * @param array $context
 *   Context using while syncing.
 */
function check_finish(array &$context) {
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Import from zip package.
 */
function entity_import($folder_name, &$context) {
  $ee = new EntityImport($folder_name);
  // Intialize variables.
  if (empty($context['sandbox'])) {
    // Sort all files and get total files count.
    $ee->sortFiles();

    $context['sandbox'] = [
      'progress' => 0,
      'counter' => 0,
          // Set total file numbers of file.
      'max' => $ee->totalFiles,
      'processed' => [],
          // Get List of file to process.
      'files' => $ee->fileList,
    ];
  }
  if (!empty($context['sandbox']['files'])) {
    $current_file = array_shift($context['sandbox']['files']);
    // Set parent of term if any.
    $context['results']['count'][] = [$current_file];
    $context['results']['folder_name'] = $folder_name;
    // Increased progress by 1.
    $context['sandbox']['progress']++;

    try {
      $split_name = explode('.', $current_file);
      $entity = $ee->readFile($current_file, $split_name[1]);

      $options = [
        '@entity_id' => ($entity) ? $entity->id() : '',
        '@bundle' => ($entity) ? $entity->bundle() : '',
        '@entity_typeid' => ($entity) ? $entity->getEntityTypeId() : '',
        '@entity_lbl' => ($entity) ? $entity->label() : '',
        '@file' => $current_file,
        '@count' => count($context['sandbox']['files']),
      ];
      // Display message to user interface.
      $context['message'] = t('File <b>@entity_typeid:@bundle--@entity_id:@entity_lbl</b> in @file in progress <b>count: @count</b>', $options);
      // Display message logger.
      \Drupal::logger('entity_import')->info('File <b>@entity_typeid--@entity_id:@entity_lbl</b> in @file in progress count: @array_count', $options);
    }
    catch (\Exception $e) {
      $message = t("[ The following error occurred:- \n@message \nin file - @file ]\n", [
        '@message' => strip_tags($e->getMessage()),
        '@file' => $current_file,
      ]);
      Util::createDataLog($message, 'entity_package_upload_', $folder_name);
    }
    $context['sandbox']['processed'][] = $current_file;
    // Need to use goto statement to simplify functionality.
  }

  check_finish($context);
}

/**
 * Fll on this function when process finished.
 *
 * @param bool $success
 *   If get success.
 * @param array $results
 *   Total result processed.
 * @param array $operations
 *   Operation that has executed.
 */
function sync_finish($success, array $results, array $operations) {
  global $base_url;
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    $ee = new EntityImport($folder_name);
    $count_arr = isset($results['count']) ? $results['count'] : [];
    // Delete folder files after import.
    if (isset($results['folder_name']) && !empty($results['folder_name'])) {
      array_map('unlink', glob($ee->getConfigFilePath() . '/' . $results['folder_name'] . "/*"));
    }
    // Get log file and path to create download link.
    $report_file_path = Settings::get('application_log_folder', '/sites/default/files/application_logs/');
    $report_file_name = 'entity_package_upload_' . date("Y_m_d_H") . '_' . $results['folder_name'] . '.txt';
    $rdl = $base_url . $report_file_path . $report_file_name;

    $singular_message = Markup::create('One file processed.. Click to <a href="' . $rdl . '">download report</a>');
    $plural_message = Markup::create('@count files processed. Click to <a href="' . $rdl . '">download report</a>');
    $message = \Drupal::translation()->formatPlural(count($count_arr), $singular_message, $plural_message);
  }
  else {
    $message = t('Finished with an error.');
  }
  \Drupal::messenger()->addMessage($message);
}
