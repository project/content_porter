<?php

namespace Drupal\content_porter\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\content_porter\EntityCommonTrait;

/**
 * Content Export.
 *
 * @Action(
 *   id = "content_export_action_base",
 *   label = @Translation("Content Export"),
 *   type = "node"
 * )
 */
class ContentExportActionBase extends ActionBase implements ContainerFactoryPluginInterface {

  use EntityCommonTrait;

  /**
   * Zip file name.
   *
   * @var string
   */
  protected $zipName;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {

    return new static($configuration, $plugin_id, $plugin_definition, $container->get('user.private_tempstore'), $container->get('session_manager'), $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function executeMultiple(array $entities) {
    $batch = [
      'title' => 'Node export',
      'operations' => [['selective_node_export', [$entities]]],
      'finished' => 'sync_finish',
      'file' => drupal_get_path('module', 'content_porter') . '/src/batchExport.inc',
    ];
    batch_set($batch);
  }

  /**
   * {@inheritdoc}
   */
  public function execute(ContentEntityInterface $entity = NULL) {
    $this->executeMultiple([$entity]);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {

    return $object->access('export', $account, $return_as_object);
  }

}
