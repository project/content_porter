<?php

namespace Drupal\content_porter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\State;
use Drupal\Core\Site\Settings as Dsettings;

/**
 * Class Settings.
 *
 * @package Drupal\content_porter\Form
 */
class Settings extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_porter_settings';
  }

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * Class constructor.
   */
  public function __construct(State $config_state) {
    $this->config = $config_state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Added sites.php to get domains list.
    $config = $this->config;
    $file_public_path = DSettings::get('file_public_path');

    $form['settings'] = [
      '#type' => 'vertical_tabs',
    ];

    // Administer setting tab.
    $form['administer'] = [
      '#type' => 'details',
      '#title' => $this->t('Entity export settings'),
      '#group' => 'settings',
    ];

    $default_package_path = (!empty($config->get('content_porter.zipFilePath'))) ? $config->get('content_porter.zipFilePath') : "sites/default/files";
    $form['administer']['zipFilePath'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity export zipFile path'),
      '#default_value' => $default_package_path,
      '#description' => $this->t('Please enter the location where package in zip format will be created: @current_path.<br>Default package location is @default_path.<br>Package file path should be relative like', ['@current_path' => $file_public_path, '@default_path' => "sites/default/files"]),
    ];

    $form['administer']['exportUsers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export Users'),
      '#default_value' => $config->get('content_porter.exportUsers'),
      '#description' => $this->t('If you export users that are referenced into any entity, then it will overwrite target environment users.'),
    ];
    $form['administer']['withoutReference'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Export entity without reference'),
      '#default_value' => $config->get('content_porter.withoutReference'),
      '#description' => $this->t('Check if you want export entity without reference entities.'),
    ];
    $form['administer']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $zipFilePath = $form_state->getValue('zipFilePath');
    $withoutReference = $form_state->getValue('withoutReference');
    $exportUsers = $form_state->getValue('exportUsers');
    try {
      $config = [
        'content_porter.zipFilePath' => $zipFilePath,
        'content_porter.withoutReference' => $withoutReference,
        'content_porter.exportUsers' => $exportUsers,
      ];

      $this->config->setMultiple($config);
      drupal_set_message($this->t('Values successfully saved'));
    }
    catch (\Exception $e) {
      drupal_set_message($e->getMessage(), 'error');
    }

  }

}
