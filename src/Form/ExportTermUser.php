<?php

namespace Drupal\content_porter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\content_porter\EntityCommonTrait;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Entity import form.
 */
class ExportTermUser extends FormBase {

  use EntityCommonTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_porter_export_term_user';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    // Fetch a list of content types.
    $contentTypes = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();
    $contentTypesList = [];

    foreach ($contentTypes as $contentType) {
      $contentTypesList[$contentType->id()] = $contentType->label();
    }

    // Fetch a list of vocabularies.
    $vocabularies = Vocabulary::loadMultiple();
    $vocabsList = [];
    foreach ($vocabularies as $vocabulary) {
      $vocabsList[$vocabulary->id()] = $vocabulary->label();
    }

    $form['entity_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select entity to export'),
      '#description' => $this->t('Please select entity to export as package.'),
      '#options' => [
        'term' => $this->t('Terms'),
        'content' => $this->t('Content'),
      ],
      '#ajax' => [
        'callback' => [$this, 'renderEntity'],
        'event' => 'change',
        'wrapper' => 'entity-render',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Rendering entity...'),
        ],
      ],
    ];

    $form['render_entity_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'entity-render'],
    ];

    if (isset($values['entity_type']) && $values['entity_type'] == 'content') {
      $form['render_entity_wrapper']['content_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Select content type'),
        '#description' => $this->t('Please select content type.'),
        '#states' => [
          'visible' => [':input[name="entity_type"]' => ['value' => 'content']],
        ],
        '#options' => $contentTypesList,
      ];
    }
    if (isset($values['entity_type']) && $values['entity_type'] == 'term') {
      $form['render_entity_wrapper']['vocabulary'] = [
        '#type' => 'select',
        '#title' => $this->t('Select vocabulary'),
        '#description' => $this->t('Select vocabulary.'),
        '#empty_option' => $this->t('Select'),
        '#options' => $vocabsList,
        '#required' => TRUE,
        '#states' => [
          'visible' => [':input[name="entity_type"]' => ['value' => 'term']],
        ],
        '#ajax' => [
          'callback' => [$this, 'getVocabularyTerms'],
          'event' => 'change',
          'wrapper' => 'taxonomy-term',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Fetching vocabulary terms...'),
          ],
        ],
      ];
    }

    $form['taxonomy_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'taxonomy-term'],
    ];

    if (isset($values['vocabulary'])) {
      $terms = $this->taxonomyTermOption($values['vocabulary']);
      if ($terms) {
        $form['taxonomy_wrapper']['terms'] = [
          '#type' => 'select',
          '#title' => $this->t('Select taxonomy'),
          '#description' => $this->t('Please select taxonomy.'),
          '#options' => $terms,
          '#default_value' => '',
          '#required' => TRUE,
          '#multiple' => TRUE,
          '#states' => [
            'visible' => [':input[name="entity_type"]' => ['value' => 'term']],
          ],
          '#attributes' => [
            'id' => 'edit-select-term',
            'style' => 'width:300px',
          ],
        ];
      }
      else {
        $form['taxonomy_wrapper']['info'] = [
          '#type' => 'label',
          '#states' => [
            'visible' => [':input[name="entity_type"]' => ['value' => 'term']],
          ],
          '#title' => $this->t('No terms available.'),
        ];
      }

    }
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * AJAX request for vocabulary term.
   */
  public function getVocabularyTerms(array &$form, FormStateInterface $form_state) {
    return $form['taxonomy_wrapper'];
  }

  /**
   * AJAX request for entity render.
   */
  public function renderEntity(array &$form, FormStateInterface $form_state) {
    return $form['render_entity_wrapper'];
  }

  /**
   * Create taxonomy term options.
   */
  public function taxonomyTermOption($vocabulary = '') {
    $taxonomy_options = [];
    if (isset($vocabulary)) {
      $trees = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadTree($vocabulary, $parent = 0, $max_depth = NULL, $load_entities = FALSE);

      foreach ($trees as $term) {
        if ($term) {
          $term_depth = '';
          for ($i = 0; $i < $term->depth; $i++) {
            $term_depth .= ' - ';
          }
          $taxonomy_options[$term->tid] = $term_depth . $term->name;
        }
      }
    }
    return $taxonomy_options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $selection = $values['entity_type'];
    $terms = isset($values['terms']) ? $values['terms'] : '';

    $batch = [];

    if ($selection == 'term' && !empty($terms)) {
      $batch = [
        'title' => t('Term export'),
        'operations' => [['entity_export', [$terms, 'taxonomy_term']]],
      ];
    }
    elseif ($selection == 'content') {
      $eq = \Drupal::entityQuery('node');
      $eq->condition('type', $values['content_type']);
      $eq->condition('status', 1);
      $nids = $eq->execute();
      $batch = [
        'title' => t('Node export'),
        'operations' => [['entity_export', [$nids, 'node']]],
      ];
    }

    if (!empty($batch)) {
      $batch['finished'] = 'sync_finish';
      $batch['file'] = drupal_get_path('module', 'content_porter') . '/src/batchExport.inc';
      batch_set($batch);
    }
  }

}
