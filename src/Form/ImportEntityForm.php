<?php

namespace Drupal\content_porter\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\content_porter\EntityCommonTrait;
use Drupal\Core\State\State;
use Drupal\Core\File\FileSystem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity import form.
 */
class ImportEntityForm extends FormBase {

  use EntityCommonTrait;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(State $config_state) {
    // $this->setConfigFilePath();
    $this->setConfigZipPath();
    $this->config = $config_state;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_porter_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['package'] = [
      '#type' => 'managed_file',
      '#title' => t('Package zip file'),
      '#size' => 20,
      '#description' => t('Please select entity package file to create entity. Package should be in zip format.'),
      '#upload_validators' => [
        'file_validate_extensions' => ['zip'],
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $package = $values['package'];
    $folder_name = time() . '_import';
    try {
      if (isset($package[0]) && $file_obj = File::load($package[0])) {
        $this->extractZip(FileSystem::realpath($file_obj->uri->value), $folder_name);

        $batch = [
          'title' => t('Entity Import'),
          'finished' => 'sync_finish',
          'file' => drupal_get_path('module', 'content_porter') . '/src/batchImport.inc',
          'operations' => [['entity_import', [$folder_name]]],
        ];
        batch_set($batch);
        $this->messenger()->addMessage(t("content successfully imported"));
      }
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage(t("Something went wrong :@message", ['@message' => $e->getMessage()]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $package = $form_state->getValue('package');

    if (empty($package)) {
      $form_state->setErrorByName('package', t('Please select the package to import.'));
    }
    if (isset($package[0]) && !File::load($package[0])) {
      $form_state->setErrorByName('package', t('Package could not get uploaded.'));
    }
  }

  /**
   * Extract zip file.
   *
   * @param mixed $file_to_open
   *   Zip file to open.
   * @param string $folder_name
   *   Foder name to extract files.
   */
  public function extractZip($file_to_open, $folder_name) {
    $zip = new \ZipArchive();
    $x = $zip->open($file_to_open);
    if ($x === TRUE) {
      $zip->extractTo($this->getConfigFilePath() . '/' . $folder_name);
      $zip->close();
    }
    else {
      die("There was a problem. Please try again!");
    }
  }

}
