<?php

/**
 * @file
 * Use to sync vocabolaries and terms to child sites.
 */

use Drupal\Core\Url;
use Drupal\content_porter\EntityExport;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Render\Markup;

/**
 * Term export batch API initiator.
 */
function entity_export($tids, $entity_type, &$context) {
  // Intialize variables.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [
      'progress' => 0,
      'entities' => $tids,
      'counter' => 0,
      'max' => count($tids),
      'processed' => [],
      'folder_name' => time(),
    ];
  }

  $key = key($context['sandbox']['entities']);
  $entity_id = $context['sandbox']['entities'][$key];
  $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);

  if (!$entity instanceof ContentEntityInterface) {
    $context['sandbox']['progress']++;
  }
  else {
    // Set parent of term if any.
    $context['results']['count'][] = $entity->id();
    $context['results']['folder_name'] = $context['sandbox']['folder_name'];
    // Increased progress by 1.
    $context['sandbox']['progress']++;
    // Display message to user interface.
    $context['message'] = t('Entity @entityType <b>@taxonomy</b> in <b>@bundle</b>', [
      '@entityType' => $entity->getEntityTypeId(),
      '@taxonomy' => $entity->label(),
      '@bundle' => $entity->bundle(),
    ]
    );
    try {
      export_entity($entity, $context);
      \Drupal::logger('entity_export')->info('Entity @entityType <b>@taxonomy</b> in <b>@bundle</b>', [
        '@entityType' => $entity->getEntityTypeId(),
        '@taxonomy' => $entity->label(),
        '@bundle' => $entity->bundle(),
      ]);
    }
    catch (\Exception $e) {
      \Drupal::logger('entity_export')->error('Error :@error @entityType <b>@taxonomy</b> in <b>@bundle</b> not processed', [
        '@error' => $e->getMessage(),
        '@entityType' => $entity->getEntityTypeId(),
        '@taxonomy' => $entity->label(),
        '@bundle' => $entity->bundle(),
      ]);
    }
    $context['sandbox']['processed'][] = $entity->id();
    // Need to use goto statement to simplify functionality.
  }
  unset($context['sandbox']['entities'][$key]);
  check_finish($context);
}

/**
 * Node export batch API initiator.
 */
function selective_node_export($entities, &$context) {
  // Intialize variables.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = [
      'progress' => 0,
      'entities' => $entities,
      'counter' => 0,
      'max' => count($entities),
      'processed' => [],
      'folder_name' => time(),
    ];
  }

  $key = key($context['sandbox']['entities']);
  $entity = $context['sandbox']['entities'][$key];

  if (!$entity instanceof ContentEntityInterface || !$entity->isPublished()) {
    $context['sandbox']['progress']++;
  }
  else {
    // Set parent of term if any.
    $context['results']['count'][] = $entity->id();
    $context['results']['folder_name'] = $context['sandbox']['folder_name'];
    // Increased progress by 1.
    $context['sandbox']['progress']++;
    // Display message to user interface.
    $context['message'] = t('Entity @entityType <b>@taxonomy</b> in <b>@bundle</b>', [
      '@entityType' => $entity->getEntityTypeId(),
      '@taxonomy' => $entity->label(),
      '@bundle' => $entity->bundle(),
    ]
    );
    try {
      (new EntityExport($context['sandbox']['folder_name']))->setContent($entity)->export();
      \Drupal::logger('entity_export')->info('Entity @entityType <b>@taxonomy</b> in <b>@bundle</b>', [
        '@entityType' => $entity->getEntityTypeId(),
        '@taxonomy' => $entity->label(),
        '@bundle' => $entity->bundle(),
      ]);
    }
    catch (\Exception $e) {
      \Drupal::logger('entity_export')->error('Error :@error @entityType <b>@taxonomy</b> in <b>@bundle</b> not processed', [
        '@error' => $e->getMessage(),
        '@entityType' => $entity->getEntityTypeId(),
        '@taxonomy' => $entity->label(),
        '@bundle' => $entity->bundle(),
      ]);
    }
    $context['sandbox']['processed'][] = $entity->id();
    // Need to use goto statement to simplify functionality.
  }
  unset($context['sandbox']['entities'][$key]);
  check_finish($context);
}

/**
 * Entity process and export into file.
 */
function export_entity($entity, $context) {

  foreach ($entity->getTranslationLanguages() as $id => $value) {
    (new EntityExport($context['sandbox']['folder_name']))->setContent($entity->getTranslation($id))->export();
  }
}

/**
 * Function to check finish status.
 *
 * @param array $context
 *   Context using while syncing.
 */
function check_finish(array &$context) {
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Fll on this function when process finished.
 *
 * @param bool $success
 *   If get success.
 * @param array $results
 *   Total result processed.
 * @param array $operations
 *   Operation that has executed.
 */
function sync_finish($success, array $results, array $operations) {
  // The 'success' parameter means no fatal PHP errors were detected. All
  // other error management should be handled using 'results'.
  if ($success) {
    $count_arr = isset($results['count']) ? $results['count'] : [];
    $folder_name = isset($results['folder_name']) ? $results['folder_name'] : '';

    if (!empty($folder_name)) {
      $ee = new EntityExport($folder_name);
      $zip_name = 'content-package_' . $folder_name . '.zip';
      // $zip_path = '/sites/default/files/' . $zip_name;.
      $zip_path_download_link = Url::fromRoute('content_porter.download-package', ['package_id' => $folder_name])->toString();
      $singular_message = Markup::create('One post processed. Click to <a href="' . $zip_path_download_link . '">download</a>.');
      $plural_message = Markup::create('@count posts processed. Click to <a href="' . $zip_path_download_link . '">download</a>.');
      // Create zip from conigurations files.
      create_zip($ee->getConfigFilePath(), $ee->getConfigZipPath(), $zip_name);
    }
    else {
      $singular_message = Markup::create('One post processed.');
      $plural_message = Markup::create('@count posts processed.');
    }

    $message = \Drupal::translation()->formatPlural(count($count_arr), $singular_message, $plural_message);
  }
  else {
    $message = t('Finished with an error.');
  }
  \Drupal::messenger()->addMessage($message);
}

/**
 * Create zip form files.
 */
function create_zip($config_path, $zip_location, $file_name) {

  // Initialize archive object.
  $zip = new \ZipArchive();
  $zip_file = $zip_location . '/' . $file_name;
  $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

  // Initialize empty "delete list".
  $filesToDelete = [];

  // Create recursive directory iterator.
  /** @var SplFileInfo[] $files */
  $files = new \RecursiveIteratorIterator(
      new \RecursiveDirectoryIterator($config_path), \RecursiveIteratorIterator::LEAVES_ONLY
  );

  foreach ($files as $file) {
    // Skip directories (they would be added automatically)
    if (!$file->isDir()) {
      // Get real and relative path for current file.
      $filePath = $file->getRealPath();
      $relativePath = substr($filePath, strlen($config_path) + 1);

      // Add current file to archive.
      $zip->addFile($filePath, $relativePath);

      // Add current file to "delete list"
      // delete it later cause ZipArchive create archive only after calling
      // close function and ZipArchive lock files until archive created)
      $filesToDelete[] = $filePath;
    }
  }

  // Zip archive will be created only after closing object.
  $zip->close();

  // Delete all files from "delete list".
  foreach ($filesToDelete as $file) {
    if ($file != $zip_file) {
      unlink($file);
    }
  }
}
